package CalculatorProject.functional;

import static org.testng.Assert.assertEquals;

import CalculatorProject.CalculatorEngine;
import CalculatorProject.Key;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.stream.Collectors;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FT_CalculatorEngine {

    private CalculatorEngine engine;

    @BeforeMethod
    public void setUp() {
        engine = new CalculatorEngine();
    }

    @Test(dataProvider = "calculatorResults")
    public void testCalculations(
            String testCaseName,
            Key[] inputs,
            Deque<Long> outputStack,
            long resultValue,
            boolean isError) {
        pressButtons(inputs);

        assertEquals(
                engine.getStack(), outputStack, testCaseName + ": Resulting stack does not match");
        assertEquals(
                engine.getCurrentValue(),
                resultValue,
                testCaseName + ": Resulting value is incorrect");
        assertEquals(engine.getIsError(), isError, testCaseName + ": Error state is incorrect");
    }

    public void pressButtons(Key... keys) {
        for (Key k : keys) {
            engine.keyPressed(k);
        }
    }

    /**
     * Provides inputs to the engine and the expected results: - Resultant stack - Current Value -
     * Is Error
     *
     * @return
     */
    @DataProvider
    public Object[][] calculatorResults() {
        return new Object[][] {
            {
                "Complete operation [2,4,*,2,+](2 * 4 + 2)",
                new Key[] {
                    Key.NUM_2, Key.ENTER, Key.NUM_4, Key.TIMES, Key.ENTER, Key.NUM_2, Key.PLUS
                },
                new LinkedList<Long>(),
                10,
                false
            },
            {
                "Complete operation with remaining input [1,5,-,2,+,9](1 - 5 + 2, 9)",
                new Key[] {
                    Key.NUM_1, Key.ENTER, Key.NUM_5, Key.MINUS, Key.ENTER, Key.NUM_2, Key.PLUS,
                    Key.ENTER, Key.NUM_9
                },
                createCalcStack(-2),
                9,
                false
            },
            {
                "Complete operation with large stack [0,12,3,45,678,9,2]",
                new Key[] {
                    Key.NUM_0, Key.ENTER, Key.NUM_1, Key.NUM_2, Key.ENTER, Key.NUM_3, Key.ENTER,
                    Key.NUM_4, Key.NUM_5, Key.ENTER, Key.NUM_6, Key.NUM_7, Key.NUM_8, Key.ENTER,
                    Key.NUM_9, Key.ENTER, Key.NUM_2
                },
                createCalcStack(9, 678, 45, 3, 12, 0),
                2,
                false
            },
            {"Invalid operator use", new Key[] {Key.PLUS}, new LinkedList<Long>(), 0, true},
            {
                "Complete operation with operators after adding everything to stack [2,4,2,+,*]((2 + 4) * 2)",
                new Key[] {
                    Key.NUM_2, Key.ENTER,
                    Key.NUM_4, Key.ENTER,
                    Key.NUM_2, Key.PLUS,
                    Key.TIMES
                },
                new LinkedList<Long>(),
                12,
                false
            },
        };
    }

    @Test(dataProvider = "keyPressedDataProvider")
    public void testKeysPressed(
            String testCaseName,
            Key keyPressed,
            LinkedList<Long> initialStack,
            long initialValue,
            boolean initialIsError,
            LinkedList<Long> expectedStack,
            long expectedValue,
            boolean expectedIsError) {
        // Arrange
        engine.setStack(initialStack);
        engine.setCurrentValue(initialValue);
        engine.setIsError(initialIsError);

        // Act
        engine.keyPressed(keyPressed);

        // Assert
        assertEquals(
                engine.getStack(), expectedStack, testCaseName + ": The stack has incorrect data");
        assertEquals(
                engine.getCurrentValue(),
                expectedValue,
                testCaseName + ": The current value is incorrect");
        assertEquals(
                engine.getIsError(),
                expectedIsError,
                testCaseName + ": The error state is incorrect");
    }

    @DataProvider
    public Object[][] keyPressedDataProvider() {
        return new Object[][] {
            /*******************
             * Test NUM keys   *
             *******************/
            // Trivial case: Zero with empty value
            {
                "Press 0 with currentValue = 0",
                Key.NUM_0,
                new LinkedList<Long>(),
                0,
                false,
                new LinkedList<Long>(),
                0,
                false
            },
            // Add initial digit
            {
                "Press digit with currentValue = 0",
                Key.NUM_1,
                new LinkedList<Long>(),
                0,
                false,
                new LinkedList<Long>(),
                1,
                false
            },
            // Add second digit
            {
                "Press digit with currentValue != 0",
                Key.NUM_2,
                new LinkedList<Long>(),
                1,
                false,
                new LinkedList<Long>(),
                12,
                false
            },
            // Add zero digit
            {
                "Press 0 with currentValue != 0",
                Key.NUM_0,
                new LinkedList<Long>(),
                1,
                false,
                new LinkedList<Long>(),
                10,
                false
            },
            /*******************
             * Test PLUS key   *
             *******************/
            // Standard addition with values in stack
            {
                "Add 3 + 8",
                Key.PLUS,
                createCalcStack(8, 2, 1),
                3,
                false,
                createCalcStack(2, 1),
                11,
                false
            },
            // Add 0
            {
                "Add 0 to existing stack",
                Key.PLUS,
                createCalcStack(32, 2, 1),
                0,
                false,
                createCalcStack(2, 1),
                32,
                false
            },
            // Add to empty stack
            {
                "Add value to empty stack",
                Key.PLUS,
                new LinkedList<Long>(),
                32,
                false,
                new LinkedList<Long>(),
                0,
                true
            },
            /*******************
             * Test MINUS key  *
             *******************/
            // Standard subtract with values in stack
            {
                "Subtract 3 from 8",
                Key.MINUS,
                createCalcStack(8, 2, 1),
                3,
                false,
                createCalcStack(2, 1),
                5,
                false
            },
            // Subtract 0
            {
                "Subtract 0 from existing stack",
                Key.MINUS,
                createCalcStack(32, 2, 1),
                0,
                false,
                createCalcStack(2, 1),
                32,
                false
            },
            // Subtract from empty stack
            {
                "Subtract value from empty stack",
                Key.MINUS,
                new LinkedList<Long>(),
                32,
                false,
                new LinkedList<Long>(),
                0,
                true
            },
            /*******************
             * Test TIMES key  *
             *******************/
            // Standard multiply with values in stack
            {
                "Multiply 4 * 8",
                Key.TIMES,
                createCalcStack(8, 2, 1),
                4,
                false,
                createCalcStack(2, 1),
                32,
                false
            },
            // Multiply by 0
            {
                "Multiply by 0",
                Key.TIMES,
                createCalcStack(32, 2, 1),
                0,
                false,
                createCalcStack(2, 1),
                0,
                false
            },
            // Multiply empty stack
            {
                "Multiply value with empty stack",
                Key.TIMES,
                new LinkedList<Long>(),
                32,
                false,
                new LinkedList<Long>(),
                0,
                true
            },
            /*******************
             * Test DIVIDE key *
             *******************/
            // Standard divide with values in stack
            {
                "Divide 32 by 4",
                Key.DIVIDE,
                createCalcStack(32, 2, 1),
                4,
                false,
                createCalcStack(2, 1),
                8,
                false
            },
            // Divide by 0
            {
                "Divide by zero",
                Key.DIVIDE,
                createCalcStack(32, 2, 1),
                0,
                false,
                createCalcStack(2, 1),
                0,
                true
            },
            // Divide empty stack
            {
                "Divide empty stack by value",
                Key.DIVIDE,
                new LinkedList<Long>(),
                32,
                false,
                new LinkedList<Long>(),
                0,
                true
            },
            /*******************
             * Test EQUALS key *
             *******************/
            // TODO: Currently not implemented
            /*******************
             * Test CLEAR key  *
             *******************/
            // Trivial case: Clear cleared engine
            {
                "Clear empty engine",
                Key.CLEAR,
                new LinkedList<Long>(),
                0,
                false,
                new LinkedList<Long>(),
                0,
                false
            },
            // Clear existing currentValue
            {
                "Clear currentValue",
                Key.CLEAR,
                new LinkedList<Long>(),
                123,
                false,
                new LinkedList<Long>(),
                0,
                false
            },
            // Clear existing stack
            {
                "Clear stack",
                Key.CLEAR,
                createCalcStack(1, 2, 3, 4, 5, 6, 7),
                0,
                false,
                new LinkedList<Long>(),
                0,
                false
            },
            // Clear error state
            {
                "Clear error",
                Key.CLEAR,
                new LinkedList<Long>(),
                0,
                true,
                new LinkedList<Long>(),
                0,
                false
            },
            // Clear all data
            {
                "Clear all",
                Key.CLEAR,
                createCalcStack(1, 2, 3, 4, 5, 6, 7),
                21398,
                true,
                new LinkedList<Long>(),
                0,
                false
            },
            /*******************
             * Test ENTER key  *
             *******************/
            // Ensure value is added to stack
            {
                "Add value to stack",
                Key.ENTER,
                new LinkedList<Long>(),
                32,
                false,
                createCalcStack(32),
                0,
                false
            },
            // Add 0 to the stack
            {
                "Add 0 to stack",
                Key.ENTER,
                new LinkedList<Long>(),
                0,
                false,
                createCalcStack(0),
                0,
                false
            },
            /*******************
             * Test NEGATE key  *
             *******************/
            // Positive value is made negative
            {
                "Negate positive",
                Key.NEGATE,
                new LinkedList<Long>(),
                32,
                false,
                new LinkedList<Long>(),
                -32,
                false
            },
            // Negative value is made positive
            {
                "Negate negative",
                Key.NEGATE,
                new LinkedList<Long>(),
                -32,
                false,
                new LinkedList<Long>(),
                32,
                false
            }
        };
    }

    public LinkedList<Long> createCalcStack(int... data) {
        return new LinkedList<>(
                Arrays.stream(data).asLongStream().boxed().collect(Collectors.toList()));
    }

    public LinkedList<Long> createCalcStack(Long... data) {
        return new LinkedList<>(Arrays.asList(data));
    }
}
