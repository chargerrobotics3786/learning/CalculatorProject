package CalculatorProject;

import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CalculatorApp extends Application {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void start(Stage stage) {
        logger.info("Starting");
        CalculatorEngine engine = new CalculatorEngine();
        CalculatorUI ui = new CalculatorUI(engine);
        ui.populateStage(stage);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
