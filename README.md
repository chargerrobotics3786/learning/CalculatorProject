# Calculator Project

This project will add functionality to a calculator so it can perform basic arithmetic.

This calculator utilizes a stack, which is a data type that stores a bunch of data (in this case, numbers)
and performs operations on them in a specific order.

## Implementing Solutions

The calculator needs to be able to display digits; add, subtract, multiply, and divide the stored digits;
clear what's currently stored; and other functions. This project is broken up into small problems, which
can be found in the `CalculatorEngine.java` class wherever there's a `// TODO:`.

Documentation for working with stacks can be found at: <https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/util/Deque.html>

## Testing Code Changes

Go to the `main()` method in `CalculatorApp.java`. There will be a "Run" button above the method. This
will pull up a calculator UI where you can test your changes. Also, there's a Gradle task named `test`. To
run the functional tests: open the WPILib Command Palette, run a command in Gradle, and run the "test"
task.
